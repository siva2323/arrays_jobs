const jobs=require('./1-arrays-jobs.cjs');
let averageSalary=(elements)=>
{
        let total=elements.reduce((accumulator,value) =>
        {
            if(value["location"] in accumulator)
            {
                accumulator[value["location"]]["sum"] +=Number(value["salary"].replaceAll("$",""));
                accumulator[value["location"]]["count"] +=1; 
            }else 
            {
                accumulator[value["location"]]={"sum":Number(value["salary"].replaceAll("$","")),"count":1};
            }
                return accumulator;
        },{})

            let answer={}
            for(let key in total)
            {
                answer[key]=total[key].sum/total[key].count;
            }
            return answer;
}
console.log(averageSalary(jobs));